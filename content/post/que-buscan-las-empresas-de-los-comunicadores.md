---
title: ¿Qué buscan las empresas de los comunicadores?
date: '2019-08-25'
publishdate: 2020-01-03T22:59:06.146Z
description: ¿Qué buscan las empresas de los comunicadores?
draft: false
author: Rod
url: cats-are-cute
categories:
  - post
Tags:
  - tag
---
Cuando estaba en tercer año empecé a buscar practicas pre-profesionales pagadas.

Antes de ello había sido voluntario en una ONG y, aunque aprendí mucho, me di cuenta de que era <strong>tiempo de aprender en un lugar diferente</strong> (donde también recibiera un salario por mi trabajo, por supuesto).

Esto no fue nada fácil. Busqué mucho y no lograba obtener el famoso "<em>Estás contratado</em>".

En una de estas entrevistas de trabajo (que siempre recordaré), me dijeron: <em>Buscamos a alguien que pueda aumentar las inscripciones de nuestra academia a través de nuestro fanpage. ¿Qué harías para lograrlo?</em>

Mi <strong>pésima respuesta</strong> (obviamente, de alguien que había manejado un fanpage meramente informativo): <em>Tendría que realizar un diagnóstico antropológico de los usuarios, revisar libros y artículos sobre cómo se comportan estos usuarios...</em>

No sé si fue por mi inexperiencia o por el hecho de que ni sabía realizar un diagnóstico antropológico (solo tenía ciertas nociones) que me dijeron: <em>Eres demasiado teórico y no creemos que estés capacitado para el puesto</em>.

Y <strong>tenían razón</strong>.

En mi hoja de vida había colocado muchos certificados de cursos de sociología, antropología e incluso charlas de economía. Es decir, nada que sirva <em>para el puesto</em>.

Al salir del lugar, empecé a pensar seriamente en esto.

Nadie me contrataba porque no había logrado nada. ¿Qué podía aportar a mi lugar de trabajo? ¿Qué tan rentable sería pagarme un sueldo? ¿Por qué elegirme a mí sobre alguien que tenía mejores capacidades?

En estos últimos 5 años trabajando como comunicador (e incluso contratando personal) logré responderme a estas preguntas. <strong>Y hoy te las contaré</strong>.

<h2>1. Las empresas no buscan talento, sino especialistas</h2>
En mis primeros años de carrera, pensaba que debía ser el mejor en todo lo que hacía.

Grave error.

Puedes ser el peor fotógrafo o diseñador, pero si logras enamorar a tus lectores con tus textos, entonces no deberías preocuparte por tomar fotos desenfocadas u odiar Illustrator. Posiblemente, la redacción publicitaria sea lo tuyo y, sobre todo, sea aquello en lo cual deberías de enfocarte.

Ahora, existen muchas más <strong>rutas de especialización</strong> que la redacción o el diseño.

Desde periodistas y productores audiovisuales hasta comunicadores en salud, existen diversos tipos de profesionales que tienen <strong>una base de estudios en comunicación</strong>.

Y aquí viene lo bueno: no necesitas (ni deberías) egresar para escoger este camino. De hecho, puedes especializarte a medida que vas avanzando en tus años de carrera.

En mi caso, empecé a desarrollar sitios web desde que estaba en segundo año.

No me considero el mejor, pero <strong>los años pesan</strong>. Luego de 5 años en el desarrollo y mantenimiento de sitios, muchos errores y frecuentes amanecidas, no diría que logré volverme talentoso, sino simplemente alguien que sabe muy bien lo que está haciendo.

¿Y el "talento"?

Pues, puede que solo sea un nombre bonito.

Siempre que tengas como objetivo el <strong>especializarte</strong>, puedes lograr muchísimo y simplemente dedicándote a una rama específica (en mi caso, la comunicación digital) durante cierto tiempo (al menos un par de años).

En la siguiente imagen verás algunas de las especialidades y disciplinas más básicas (obviamente, hay <strong>muchísimas</strong> <strong>más</strong>).

<img class="irc_mi aligncenter" src="https://image.slidesharecdn.com/cienciasdelacomunicacin-140306191053-phpapp02/95/ciencias-de-la-comunicacin-4-638.jpg?cb=1394133099" alt="Resultado de imagen para especialidades de la comunicacion" width="542" height="407" />
<h2>2. Los logros valen más que los certificados</h2>
Haber conseguido que miles de personas visiten con frecuencia los contenidos de tu web debido a su alta calidad vale más que cualquier certificado en marketing de contenidos.

¿Cómo una empresa puede saber si eres bueno en cierto campo?

¿Por tus certificados? Estos son un <strong>indicador bastante relativo y superficial</strong>. Muchas academias e institutos los ofrecen por asistencia, pero nada garantiza que realmente hayas aprendido.

Incluso si aprendiste a cabalidad, ¿qué le asegura a una empresa que apliques de forma correcta estos conocimientos?

Como todo en la vida, hay algunos casos excepcionales. Por ejemplo, algunas universidades poseen altos estándares de enseñanza, incluyen aplicaciones prácticas y exámenes finales. Sin embargo, <strong>no son el caso general</strong>.

Una de las mejores formas de saber si eres un buen profesional es a través de tus logros.

Por ejemplo: <em>He gestionado una campaña en Google Ads que ha generado 231 leads con $100, de los cuales 75 han realizado una compra efectiva. Esto ha generado un aumento del 20% de ventas en la empresa donde laboré</em>.

Este tipo de casos son una <strong>carta de presentación</strong>. En lugar de tener una hoja de vida de cinco páginas con certificados inútiles, trabaja para llenarla de logros relevantes. Solo mira este CV de una sola página:

<img class="aligncenter wp-image-2741" src="https://todosobrecomunicacion.com/wp-content/uploads/2019/11/marissa-mayer-resume.jpg" alt="" width="1200" height="1702" />
<h2>3. Crea soluciones, no problemas</h2>
Supongamos que contratas a un diseñador publicitario y que, cada cinco minutos, te pregunta:<em> ¿Está bien este color? ¿Te parece bien esta forma? ¿Están bien las ilustraciones?</em>

Mi respuesta a estas tres preguntas es: <strong>tú deberías saberlo</strong>.

A nadie le gusta que lo molesten. No solo desconcentra, sino que también implica una pérdida de tiempo.

Un profesional ideal entrega el producto final y el jefe o supervisor brinda algunas pocas observaciones que pueden ser subsanadas sin mucha dificultad.

En el peor de los casos, las correcciones pueden ser estructurales, pero tienen el objetivo de mejorar radicalmente en gran medida el producto final.

Sea que estés empezando o que ya tengas ciertos años de experiencia, <strong>debes procurar resolver todos los problemas que enfrentes</strong>. Busca la solución hasta donde puedas, pero tampoco inventes la pólvora (eso es pésimo para la gestión del tiempo).

Supongamos que debes encontrar una solución para un problema de configuración de Facebook Ads. Lo primero es dedicarle entre media y una hora a intentar solucionarlo por ti mismo. Si no puedes, indícale a tu supervisor o jefe: <em>He tenido este problema de configuración y ya le dediqué cuarenta minutos. ¿Es posible puedas ayudarme a encontrar solución o consideras mejor que siga investigando por mi cuenta?</em>

Al proponerle esto a tu jefe, <strong>le das opción a que</strong>: 1) te dé la solución del problema, 2) busque contigo la solución, 3) te encargue que sigas buscando la solución, 4) te indique que dejes de lado la investigación dado que no es relevante y 5) te indique que suspendas temporalemente la búsqueda debido a que existen otras prioridades.

<img class="attachment-full wp-post-image aligncenter" src="https://www.atrevia.com/wp-content/uploads/2018/11/Comunicacion-rocio-perez.jpg" sizes="(max-width: 800px) 100vw, 800px" srcset="https://www.atrevia.com/wp-content/uploads/2018/11/Comunicacion-rocio-perez.jpg 800w, https://www.atrevia.com/wp-content/uploads/2018/11/Comunicacion-rocio-perez-768x480.jpg 768w, https://www.atrevia.com/wp-content/uploads/2018/11/Comunicacion-rocio-perez-250x156.jpg 250w, https://www.atrevia.com/wp-content/uploads/2018/11/Comunicacion-rocio-perez-550x344.jpg 550w, https://www.atrevia.com/wp-content/uploads/2018/11/Comunicacion-rocio-perez-288x180.jpg 288w, https://www.atrevia.com/wp-content/uploads/2018/11/Comunicacion-rocio-perez-480x300.jpg 480w" alt="" width="800" height="500" />
<h2>Conclusiones</h2>
Aprender skills técnicos de acuerdo a tu especialidad, obtener logros y crear soluciones son puntos clave para encontrar trabajo como comunicador.

Sin embargo, lo más importante es que reflexiones mucho sobre <strong>qué es aquello que las empresas quieren de uno</strong>. Y claro, no hay una respuesta única dado que cada sector y rubro es diferente.

¡Espero que este artículo te ayude en tu camino profesional! ¿Tienes alguna duda o pregunta? Escríbenos tus comentarios.
