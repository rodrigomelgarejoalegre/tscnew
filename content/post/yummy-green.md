---
title: "First"
description: "first post with Hugo website engine"
date: "2015-08-18"
categories:
    - "post"
tags:
    - "meta"
    - "test"
cardthumbimage: "/images/default.jpg" #optional: default solid color if unset
cardheaderimage: "/images/default.jpg" #optional: default solid color if unset
cardbackground: "#263238" #optional: card background color; only shows when no image specified
#cardtitlecolor: "#fafafa" #optional: can be changed to make text visible over card image
"author":
    name: "Firstname Lastname"
    description: "Writer of stuff"
    website: "http://example.com/"
    email: "firstname@example.com"
    twitter: "https://twitter.com/"
    github: "https://github.com/"
    image: "/images/avatar-64x64.png"
draft: false
---

This is my post. Rod

{{< figure src="/img/cats.webp" caption="Cats and Dog" >}}

{{< figure src="https://image.businessinsider.com/5b69ca9c0ce5f533278b4730?width=1300&format=jpeg&auto=webp" >}}